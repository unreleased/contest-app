<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = DB::table('users')->insertGetId([
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        
        $allRole =  \App\Role::all();
        
        //assign user a random role
        $allRole =  DB::table('roles')->select('id')->get();
        $randItem = rand(0, count($allRole)-1);
        $randRoleId = $allRole[$randItem]->id;

        DB::table('role_user')->insert([
            'user_id' => $userId,
            'role_id' => $randRoleId,
        ]);

        $role = \App\Role::find($randRoleId);
        
        if ($role->name=='contest_owner') {
            //if user is a contest owner create a contest for him/her
            DB::table('contests')->insert([
                'name' => str_random(10),
                'owner' => $userId,
            ]);
        } elseif ($role->name=='team_owner') {
            //if user is a team owner create a team for him/her
            DB::table('teams')->insert([
                'name' => str_random(10),
                'owner' => $userId,
            ]);
        } elseif ($role->name=='player') {
            //if user is a team player create a team for him/her
            $teamId = DB::table('teams')->insertGetId([
                'name' => str_random(10),
                'owner' => $userId,
            ]);
            DB::table('team_user')->insert([
                'team_id' => teamId,
                'user_id' => $userId,
            ]);
        }
    }
}
