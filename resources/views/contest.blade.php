<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" style="margin-top:10px">
		<div class="row">
			<div class="card" style="width: 18rem;" id="contestList">
				<div v-if="userType==1">
					<ul class="list-group list-group-flush">
						<li class="list-group-item">Contest 1</li>
						<li class="list-group-item">Contest 2</li>
						<li class="list-group-item">Contest 3</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="js/contest.js"></script>