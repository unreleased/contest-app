

## Installation Guide

- mkdir folder
- cd folder
- git clone https://unreleased@bitbucket.org/unreleased/contest-app.git
- cd contest-app
- cp .env.example .env
- config your .env file
- php artisan key:generate
- php artisan migrate 
- php artisan db:seed

## Login Access
- email    : pick user email from users table
- password : secret