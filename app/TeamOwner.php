<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamOwner extends Model
{
    public function teams()
    {
        return $this->hasMany('Team');
    }
}
