<?php

namespace App\Http\Controllers;

use App\ContestOwner;
use Illuminate\Http\Request;

class ContestOwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContestOwner  $contestOwner
     * @return \Illuminate\Http\Response
     */
    public function show(ContestOwner $contestOwner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContestOwner  $contestOwner
     * @return \Illuminate\Http\Response
     */
    public function edit(ContestOwner $contestOwner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContestOwner  $contestOwner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContestOwner $contestOwner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContestOwner  $contestOwner
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContestOwner $contestOwner)
    {
        //
    }
}
